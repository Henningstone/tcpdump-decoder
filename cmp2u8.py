#!/usr/bin/python3
import sys


quiet = False


def dbg(output=""):
    if not quiet:
        print(output)


def canonicalize(string: str) -> str:
    string = string.replace('"', '')
    string = string.replace(' \\', '')
    string = string.replace('\n', '')
    string = string.replace('0x', '')
    string = string.replace('\\x', '')
    return string


def reassemble(lines: []) -> str:
    string = ""
    for l in lines:
        string += l
    return string


def main():
    global quiet
    if len(sys.argv) > 1:
        if sys.argv[1] == "-q":
            quiet = True

    dbg("Pakete mit ; getrennt eingeben:")
    data = sys.stdin.readlines()
    asm = reassemble(data)
    split = asm.split(';')
    p1 = canonicalize(split[0])
    p2 = canonicalize(split[1])

    if len(p1) != len(p2):
        dbg("packet size does not match ({} != {})".format(len(p1), len(p2)))
        return 1

    result = ""
    num_replaced = 0
    for i in range(0, len(p1), 2):
        byte1 = p1[i:i+2]
        byte2 = p2[i:i+2]
        if byte1 == byte2:
            result += "\\x" + byte1
        else:
            result += "\\u8"
            num_replaced += 1

    dbg()
    dbg("Replaced bytes: {} / {}".format(num_replaced, int(len(p1)/4)))
    print(result)


if __name__ == '__main__':
    exit(main())
