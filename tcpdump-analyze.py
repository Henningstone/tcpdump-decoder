#!/usr/bin/python3
import sys
import io

# tcpdump: 01:09:51.899156 IP (tos 0x0, ttl 117, id 27794, offset 0, flags [none], proto UDP (17), length 49)
#    217.80.224.83.63166 > 31.186.250.72.8101: UDP, payload 21
#    0x0000:  4500 0031 6c92 0000 7511 0583 d950 e053  E..1l...u....P.S
#    0x0010:  1fba fa48 f6be 1fa5 001d d39a 8019 0168  ...H...........h
#    0x0020:  c148 bac1 d1d2 0c8e b6b6 251a 048b 78c5  .H........%...x.
#    0x0030:  0d

BASIC_IP_HEADER_LEN = 20  # bytes

# TODO: teeworlds packet decoding
teeworlds_serverbrowse_data = {
    "SERVERBROWSE_HEARTBEAT": [255, 255, 255, 255, ord('b'), ord('e'), ord('a'), ord('2')],

    "SERVERBROWSE_GETLIST": [255, 255, 255, 255, ord('r'), ord('e'), ord('q'), ord('2')],
    "SERVERBROWSE_LIST": [255, 255, 255, 255, ord('l'), ord('i'), ord('s'), ord('2')],

    "SERVERBROWSE_GETCOUNT": [255, 255, 255, 255, ord('c'), ord('o'), ord('u'), ord('2')],
    "SERVERBROWSE_COUNT": [255, 255, 255, 255, ord('s'), ord('i'), ord('z'), ord('2')],

    "SERVERBROWSE_GETINFO": [255, 255, 255, 255, ord('g'), ord('i'), ord('e'), ord('3')],
    "SERVERBROWSE_INFO": [255, 255, 255, 255, ord('i'), ord('n'), ord('f'), ord('3')],

    "SERVERBROWSE_GETINFO64": [255, 255, 255, 255, ord('f'), ord('s'), ord('t'), ord('d')],
    "SERVERBROWSE_INFO64": [255, 255, 255, 255, ord('d'), ord('t'), ord('s'), ord('f')],

    "SERVERBROWSE_FWCHECK": [255, 255, 255, 255, ord('f'), ord('w'), ord('?'), ord('?')],
    "SERVERBROWSE_FWRESPONSE": [255, 255, 255, 255, ord('f'), ord('w'), ord('!'), ord('!')],
    "SERVERBROWSE_FWOK": [255, 255, 255, 255, ord('f'), ord('w'), ord('o'), ord('k')],
    "SERVERBROWSE_FWERROR": [255, 255, 255, 255, ord('f'), ord('w'), ord('e'), ord('r')],
}


should_dump = False
dump_filename = ""

all_data = ""


def parse_lines(lines: [str]):
    global all_data
    result = ""
    for line in lines:
        line = line.strip()
        if line[:2] != "0x":
            continue
        line = line[9:]
        for i in range(8):
            result += line[:4]
            line = line[5:]
    all_data = result


def get_hex(num_nibbles):
    if num_nibbles == 0:
        return ""
    global all_data
    result = all_data[:num_nibbles]
    all_data = all_data[num_nibbles:]
    return result


def get_data(num_nibbles):
    hex_str = get_hex(num_nibbles)
    return int(hex_str, 16)


def byte_to_char(code: int) -> chr:
    if 0x20 <= code < 0x7F:
        return chr(code)
    else:
        return '.'


class IPHeader:
    def __init__(self):
        self.Version = get_data(1)
        self.IHL = get_data(1)
        self.TOS = get_data(2)
        self.TotalLength = get_data(4)
        self.Identification = get_data(4)
        self.Flags = get_data(1)
        self.FragmentOffset = get_data(3)
        self.TTL = get_data(2)
        self.Protocol = get_data(2)
        self.HeaderChecksum = get_data(4)
        self.SourceAddressBin = get_data(8)
        self.SourceAddress = str((self.SourceAddressBin & 0xFF000000) >> 3 * 8) + '.' \
                           + str((self.SourceAddressBin & 0x00FF0000) >> 2 * 8) + '.' \
                           + str((self.SourceAddressBin & 0x0000FF00) >> 1 * 8) + '.' \
                           + str((self.SourceAddressBin & 0x000000FF) >> 0 * 8)
        self.DestAddressBin = get_data(8)
        self.DestAddress = str((self.DestAddressBin & 0xFF000000) >> 3 * 8) + '.' \
                         + str((self.DestAddressBin & 0x00FF0000) >> 2 * 8) + '.' \
                         + str((self.DestAddressBin & 0x0000FF00) >> 1 * 8) + '.' \
                         + str((self.DestAddressBin & 0x000000FF) >> 0 * 8)
        self.Options = get_hex(((self.IHL * 32) / 8 - BASIC_IP_HEADER_LEN) * 2)

    def __str__(self) -> str:
        return """Version = {}
IHL = {}
TOS = {}
TotalLength = {}
Identification = {}
Flags = {}
FragmentOffset = {}
TTL = {}
Protocol = {}
HeaderChecksum = {}
SourceAddress = {} (0x{:02X})
DestAddress = {} (0x{:02X})
Options = '{}'""".format(
            self.Version,
            self.IHL,
            self.TOS,
            self.TotalLength,
            self.Identification,
            self.Flags,
            self.FragmentOffset,
            self.TTL,
            self.Protocol,
            self.HeaderChecksum,
            self.SourceAddress, self.SourceAddressBin,
            self.DestAddress, self.DestAddressBin,
            self.Options,
        )


class UDPHeader:
    def __init__(self):
        self.SourcePort = get_data(4)
        self.DestPort = get_data(4)
        self.Length = get_data(4)
        self.Checksum = get_data(4)

    def __str__(self):
        return """SourcePort = {}
DestPort = {}
Length = {}
Checksum = {}""".format(
            self.SourcePort,
            self.DestPort,
            self.Length,
            self.Checksum,
        )


class UserData:
    def __init__(self):
        global all_data
        global should_dump
        if should_dump:
            try:
                f1 = io.open(dump_filename + '.payload', 'wb')
                f2 = io.open(dump_filename + '.txt', 'w')
            except IOError as e:
                print("Failed to open dump file: " + str(e))
                should_dump = False

        self.data_bytes = 0
        self.data_str = ""
        line_ascii = ""
        for i in range(0, len(all_data), 2):
            # prepender
            if (i/2) % 8 == 0:
                self.data_str += "0x%04x: " % i

            next_byte = all_data[i:i+2]
            self.data_str += next_byte

            next_byte = int(next_byte, 16)
            if should_dump:
                self.data_bytes += 1
                dmp = bytes([next_byte])
                f1.write(dmp)
                f2.write("\\x%02x" % next_byte)
            char = chr(next_byte)
            if (lambda s: len(s) == len(s.encode()))(char):
                line_ascii += byte_to_char(next_byte) + " "
            else:
                line_ascii += ". "

            # appender
            if (i/2+1) % 8 == 0:
                self.data_str += "  " + line_ascii + "\n"
                line_ascii = ""
            else:
                self.data_str += " "

        if should_dump:
            f1.close()
            f2.close()

        # handle the rest
        padding = int(len(all_data)/2) % 8  # FIXME
        for i in range(padding):
            self.data_str += "  "
        self.data_str += line_ascii + "\n"

    def __str__(self):
        return self.data_str


class Packet:
    def __init__(self, lines):
        parse_lines(lines)
        self.ip_header = IPHeader()
        self.udp_header = UDPHeader()
        self.UserData = UserData()

    def __str__(self) -> str:
        result = "# IP Header\n"
        result += str(self.ip_header)
        result += "\n\n"
        result += "# UDP Header\n"
        result += str(self.udp_header)
        result += "\n\n"
        result += "# User Data\n"
        result += str(self.UserData)
        if should_dump:
            result += "\n"
            result += "# Info\n"
            result += "dumped payload to " + dump_filename + ".payload/txt"
        return result


def main():
    global should_dump
    global dump_filename
    if len(sys.argv) > 2:
        if sys.argv[1] == "-d":
            should_dump = True
            dump_filename = sys.argv[2]

    lines = sys.stdin.readlines()

    packet = Packet(lines)
    print(str(packet))

    if not should_dump:
        return 0

    # create hping command
    frag_args = ""
    if packet.ip_header.Flags & (1 << (3-1)) != 0:
        frag_args += "--dontfrag "
    if packet.ip_header.Flags & (1 << (3-2)) != 0:
        frag_args += "--morefrag "
    if packet.ip_header.FragmentOffset != 0:
        frag_args += "--fragoff {} ".format(packet.ip_header.FragmentOffset)
    hping_cmd = "hping3 {} -p {} -q --udp --rand-source --ttl $(($RANDOM % 256 + 32)) {}-d {} --file {}.payload -i u10".format(
        packet.ip_header.DestAddress, packet.udp_header.DestPort,
        frag_args,
        packet.UserData.data_bytes, dump_filename
    )

    print("\nRecreate the attack:\n" + hping_cmd)


if __name__ == '__main__':
    exit(main())
